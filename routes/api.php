<?php

header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");


//** start user controller **//
Route::group( ['namespace' => 'API'], function() {
    //route need to lang
    Route::group( ['middleware' => ['lang']], function() {
        // sign in route
        Route::post('/signup'               ,'UsersController@signUp');
        //login
        Route::post('/signin'               ,'UsersController@signIn');
        //logout
        Route::post('/logout'               ,'UsersController@logout');
        //update profile
        Route::post('/update-profile'       ,'UsersController@updateProfile');
        //change password
        Route::post('/change_password'       ,'UsersController@changePassword');
        //Rate
        Route::post('/rate_action'          ,'UsersController@postRate');
        //get user
        Route::post('/get_user'             ,'UsersController@getUser');
        //chnage langue
        Route::post('/chnage_lang'          ,'UsersController@ChnageLang');
        //complation
        Route::post('/complation'          ,'UsersController@complation');
        //contact us
        Route::post('/contact-us'          ,'UsersController@ContactUs');
        //forget password
        Route::post('/forgetPassword'      ,'UsersController@forgotPassword');
        //forget password
        Route::post('/reset-password'      ,'UsersController@resetPassword');
        //resend password
        Route::post('/resend-code'         ,'UsersController@ResendCode');
    });

});
//end user controller
//** start basicinfoControler **//
Route::group( ['namespace' => 'API'], function() {
    //route need to lang
    Route::group( ['middleware' => ['lang']], function() {
        //copy right
        Route::any('/get_copy'             ,'BasicInfoController@GetCopyRight')->middleware('AuthApi');;
        //copy desc
        Route::any('/get_about'            ,'BasicInfoController@aboutUs');
        // get transpernat
        Route::any('/get_transports'       ,'BasicInfoController@GetTransports');


    });

});
//end user basicinfoControler

//** start ChatControler **//
Route::group( ['namespace' => 'API'], function() {
    //route need to lang
    Route::group( ['middleware' => ['lang']], function() {
        //save
        Route::post('/send-message'         ,'ChatController@saveMessage');
        // get all mesage in chat
        Route::post('/chat-messages'        ,'ChatController@getChatMessages');
        // delete chat
        Route::post('/delete-chat'          ,'ChatController@deleteChatRom');
        // get chats
        Route::post('/get-chats'           ,'ChatController@getChatRoms');
        // chat message
        // get chats
        Route::post('/get-allMessags'      ,'ChatController@getChatMessagesById');

    });

});
//end user ChatControler
//** start ReservationController **//
Route::group( ['namespace' => 'API'], function() {
    //route need to lang
    Route::group( ['middleware' => ['lang']], function() {
        //make reservation
        Route::any('make_reservation'             ,'ReservationController@saveReservation');
        //finsh reservation
        Route::any('finsh_reservation'             ,'ReservationController@finsihReservation');
        //canel reservation
        Route::any('cancel_reservation'             ,'ReservationController@cancelReservation');

    });

});
//end user ReservationController
//** start trips controller **//
Route::group( ['namespace' => 'API'], function() {
    //route need to lang
    Route::group( ['middleware' => ['lang']], function() {
        /************ trips ***********/
        //add trip
        Route::post('/add-trip'         ,'ServiceController@addTrip');

        /************ end ************/
        /************ loads ***********/
        //add load
        Route::post('/add-load'         ,'ServiceController@addLoad');
        /************ end ************/
        //edit service
        Route::post('/update-service'       ,'ServiceController@editService');
        //delete service
        Route::post('/delete-service'       ,'ServiceController@deleteService');
        //get service
        Route::post('/get-service'         ,'ServiceController@getService');
        //get service
        Route::post('/get-services'         ,'ServiceController@getServices');

    });

});
//end trips controller
//** Start SettingController**//
Route::group( ['namespace' => 'API'], function() {
    Route::any('phone-keys'         ,'SettingController@phoneKeys');
    Route::any('sign-up'            ,'AuthController@signUp');
    Route::any('sign-in'            ,'AuthController@signIn');
    Route::any('forget-password'    ,'AuthController@forgetPassword');
    Route::any('update-password'    ,'AuthController@updatePassword');
});
//** End SettingController**//

Route::group(['middleware' => ['mobile'] , 'namespace' => 'API'], function() {

         Route::any('edit-profile'            ,'AuthController@editProfile');
         Route::any('edit-password'           ,'AuthController@editPassword');

});