<?php

/*---------------------------------Start Of FrontEnd--------------------------*/
Route::get('/',"DashBoardController@home");



/*---------------------------------End Of FrontEnd--------------------------*/



/*---------------------------------Start Of DashBoard--------------------------*/

Route::group(['prefix'=>'admin','middleware'=>['auth','Manager','checkRole','smtpAndFcmConfig']],function(){
	/*Start Of DashBoard Controller (Intro Page)*/
	Route::get('dashboard',[
		'uses'  =>'DashBoardController@Index',
		'as'    =>'dashboard',
		'icon'  =>'<i class="icon-home4"></i>',
		'title' =>'الرئيسيه'
		]);


	/*------------ Start Of ContactUsController ----------*/

	#messages page
	Route::get('inbox-page',[
		'uses' =>'ContactUsController@InboxPage',
		'as'   =>'inbox',
		'title'=>'الرسائل',
		'icon' =>'<i class="icon-inbox-alt"></i>',
		'child' =>['showmessage','deletemessage','sendsms','sendemail']
	]);

	#show message page
	Route::get('show-message/{id}',[
		'uses'=>'ContactUsController@ShowMessage',
		'as'  =>'showmessage',
		'title'=>'عرض الرساله'
	]);

	#send sms
	Route::post('send-sms',[
		'uses' =>'ContactUsController@SMS',
		'as'   =>'sendsms',
		'title'=>'ارسال SMS'
	]);

	#send email
	Route::post('send-email',[
		'uses' =>'ContactUsController@EMAIL',
		'as'   =>'sendemail',
		'title'=>'ارسال Email'
	]);

	#delete message
	Route::post('delete-message',[
		'uses' =>'ContactUsController@DeleteMessage',
		'as'   =>'deletemessage',
		'title'=>'حذف الرساله'
	]);

	/*------------ End Of ContactUsController ----------*/

	/*------------ End Of UsersController ----------*/

	#users list
	Route::get('users',[
		'uses' =>'UsersController@Users',
		'as'   =>'users',
		'title'=>'الاعضاء', 
		'icon' =>'<i class="icon-vcard"></i>',
		'child'=>[
			'adduser',
			'updateuser',
			'deleteuser',
			'emailallusers',
			'smsallusers',
			'notificationallusers',
			'sendcurrentemail',
			'sendcurrentsms',
			'sendcurrentnotification'
		]
	]);

	#add user
	Route::post('add-user',[
		'uses' =>'UsersController@AddUser',
		'as'   =>'adduser',
		'title'=>'اضافة عضو'
	]);

	#update user
	Route::post('update-user',[
		'uses' =>'UsersController@UpdateUser',
		'as'   =>'updateuser',
		'title'=>'تحديث عضو'
	]);

	#delete user
	Route::post('delete-user',[
		'uses' =>'UsersController@deleteUser',
		'as'   =>'deleteuser',
		'title'=>'حذف عضو'
	]);

	#email for all users
	Route::post('email-users',[
		'uses' =>'UsersController@EmailMessageAll',
		'as'   =>'emailallusers',
		'title'=>'ارسال email للجميع'
	]);

	#sms for all users
	Route::post('sms-users',[
		'uses' =>'UsersController@SmsMessageAll',
		'as'   =>'smsallusers',
		'title'=>'ارسال sms للجميع'
	]);

	#notification for all users
	Route::post('notification-users',[
		'uses' =>'UsersController@NotificationMessageAll',
		'as'   =>'notificationallusers',
		'title'=>'ارسال notification للجميع'
	]);

	#send email for current user
	Route::post('send-current-email',[
		'uses' =>'UsersController@SendEmail',
		'as'   =>'sendcurrentemail',
		'title'=>'ارساله email لعضو'
	]);

	#send sms for current user
	Route::post('send-current-sms',[
		'uses' =>'UsersController@SendSMS',
		'as'   =>'sendcurrentsms',
		'title'=>'ارساله sms لعضو'
	]);

	#send notification for current user
	Route::post('send-current-notification',[
		'uses' =>'UsersController@SendNotification',
		'as'   =>'sendcurrentnotification',
		'title'=>'ارساله notification لعضو'
	]);
	/*------------ End Of UsersController ----------*/



	/*------------ start Of PermissionsController ----------*/
	#permissions list
	Route::get('permissions-list',[
		'uses' =>'PermissionsController@PermissionsList',
		'as'   =>'permissionslist',
		'title'=>'قائمة الصلاحيات',
		'icon' =>'<i class="icon-safe"></i>',
		'child'=>[
			'addpermissionspage',
			'addpermission',
			'editpermissionpage',
			'updatepermission',
			'deletepermission'
		]
	]);

	#add permissions page
	Route::get('permissions',[
		'uses' =>'PermissionsController@AddPermissionsPage', 
		'as'   =>'addpermissionspage',
		'title'=>'اضافة صلاحيه',

	]);

	#add permission
	Route::post('add-permission',[
		'uses' =>'PermissionsController@AddPermissions',
		'as'   =>'addpermission',
		'title' =>'تمكين اضافة صلاحيه'
	]);

	#edit permissions page
	Route::get('edit-permissions/{id}',[
		'uses' =>'PermissionsController@EditPermissions',
		'as'   =>'editpermissionpage',
		'title'=>'تعديل صلاحيه'
	]);

	#update permission
	Route::post('update-permission',[
		'uses' =>'PermissionsController@UpdatePermission',
		'as'   =>'updatepermission',
		'title'=>'تمكين تعديل صلاحيه'
	]);

	#delete permission
	Route::post('delete-permission',[
		'uses'=>'PermissionsController@DeletePermission',
		'as'  =>'deletepermission',
		'title' =>'حذف صلاحيه'
	]);

	/*------------ End Of PermissionsController ----------*/



	/*------------ Start Of SettingController ----------*/

	#setting page
	Route::get('setting',[
		'uses' =>'SettingController@Setting',
		'as'   =>'setting',
		'title'=>'الاعدادات',
		'icon' =>'<i class="icon-wrench"></i>',
		'child'=>[
			'addsocials',
			'updatesocials',
			'dddd',
			'updatesmtp',
			'updatesms',
			'updateonesignal',
			'updatefcm',
			'updatesitesetting',
			'updateseo',
			'updatesitecopyright',
			'updateemailtemplate',
			'updategoogleanalytics',
			'updatelivechat'
		]
	]);

	#add socials media
	Route::post('add-socials',[
		'uses' =>'SettingController@AddSocial',
		'as'   =>'addsocials',
		'title'=>'اضافة مواقع التواصل'
	]);

	#update socials media
	Route::post('update-socials',[
		'uses' =>'SettingController@UpdateSocial',
		'as'   =>'updatesocials',
		'title'=>'تحديث مواقع التواصل'
	]);

	#delete social
	Route::post('delete-social',[
		'uses' =>'SettingController@DeleteSocial',
		'as'   =>'deletesocial',
		'title'=>'حذف مواقع التاوصل'
	]);

	#update SMTP
	Route::post('update-smtp',[
		'uses' =>'SettingController@SMTP',
		'as'   =>'updatesmtp',
		'title'=>'تحديث SMTP'
	]);

	#update SMS
	Route::post('update-sms',[
		'uses' =>'SettingController@SMS',
		'as'   =>'updatesms',
		'title'=>'تحديث SMS'
	]);

	#update OneSignal
	Route::post('update-onesignal',[
		'uses' =>'SettingController@OneSignal',
		'as'   =>'updateonesignal',
		'title'=>'تحديث OneSignal'
	]);

	#update FCM
	Route::post('update-FCM',[
		'uses' =>'SettingController@FCM',
		'as'   =>'updatefcm',
		'title'=>'تحديث FCM'
	]);

	#update SiteSetting
	Route::post('update-sitesetting',[
		'uses' =>'SettingController@SiteSetting',
		'as'   =>'updatesitesetting',
		'title'=>'تحديث الاعدادات العامه'
	]);

	#update SEO
	Route::post('update-seo',[
		'uses' =>'SettingController@SEO',
		'as'   =>'updateseo',
		'title'=>'تحديث SEO'
	]);

	#update footerCopyRight
	Route::post('update-sitecopyright',[
		'uses' =>'SettingController@SiteCopyRight',
		'as'   =>'updatesitecopyright',
		'title'=>'تحديث حقوق الموقع'
	]);

	#update email template
	Route::post('update-emailtemplate',[
		'uses' =>'SettingController@EmailTemplate',
		'as'   =>'updateemailtemplate',
		'title'=>'تحديث قالب الايميل'
	]);

	#update api google analytics
	Route::post('update-google-analytics',[
		'uses' =>'SettingController@GoogleAnalytics',
		'as'   =>'updategoogleanalytics',
		'title'=>'تحديث google analytics'
	]);

	#update api live chat
	Route::post('update-live-chat',[
		'uses' =>'SettingController@LiveChat',
		'as'   =>'updatelivechat',
		'title'=>'تحديث live chat'
	]);

	/*------------ End Of SettingController ----------*/

    /*------------ start Of TransportController  ----------*/
    Route::get('transports',[
        'uses' =>'TransportController@transports',
        'as'   =>'transports',
        'icon' =>'<i class="icon-calculator"></i>',
        'title'=>'قائمة وسائل النقل',
        'child'=>['add-transport','delete-transport','edit-transport']
    ]);

    #add
    Route::post('add-transport',[
        'uses' =>'TransportController@add',
        'as'   =>'add-transport',
        'title'=>'اضافة وسيلة جديد',
    ]);

    #edit
    Route::post('edit-transport',[
        'uses' =>'TransportController@edit',
        'as'   =>'edit-transport',
        'title'=>'تعديل وسيلة',
    ]);

    #delete
    Route::post('delete-transport',[
        'uses' =>'TransportController@delete',
        'as'   =>'delete-transport',
        'title'=>'حذف وسيلة',
    ]);
    /*------------ End Of TransportController ----------*/
    /*------------ start Of ServiceController ----------*/
    #trips list
    Route::get('trips',[
        'uses' =>'ServiceController@trips',
        'as'   =>'trips',
        'title'=>'قائمة الرحلات',
        'subTitle'=> ' الرحلات الحالية ',
        'icon' =>'<i class="glyphicon glyphicon-shopping-cart"></i>',
        'subIcon' =>'<i class="fa fa-cart-plus"></i>',
        'child'=>[
            'finsh-trips',
            'reserve-trips',
            'old-trips',
            'show-trips'
        ]
    ]);
    // new trips
    Route::get('new-trips',[
        'uses' =>'ServiceController@newTrips',
        'as'   =>'new-trips',
        'title'=>' رحلات جديده ',
        'icon' =>'<i class="fa fa-truck"></i>',
        'hasFather' => true
    ]);
    // reservation trips
    Route::get('reserve-trips',[
        'uses' =>'ServiceController@reserveTrips',
        'as'   =>'reserve-trips',
        'title'=>' رحلات محجوزه ',
        'icon' =>'<i class="fa fa-truck"></i>',
        'hasFather' => true
    ]);
    Route::get('old-trips',[
        'uses' =>'ServiceController@oldTrips',
        'as'   =>'old-trips',
        'title'=>' رحلات قديمه ',
        'icon' =>'<i class="fa fa-truck"></i>',
        'hasFather' => true
    ]);
    // show trips
    Route::get('show-trips/{id}',[
        'uses' =>'ServiceController@showtrip',
        'as'   =>'show-trips',
        'title'=>' عرض رحله ',
    ]);
    //loads
    #trips list
    Route::get('loads',[
        'uses' =>'ServiceController@loads',
        'as'   =>'loads',
        'title'=>'قائمة الحمولات',
        'subTitle'=> ' الحمولات الحالية ',
        'icon' =>'<i class="glyphicon glyphicon-shopping-cart"></i>',
        'subIcon' =>'<i class="fa fa-cart-plus"></i>',
        'child'=>[
            'finsh-loads',
            'reserve-loads',
            'old-loads',
            'show-loads'
        ]
    ]);
    // new trips
    Route::get('new-loads',[
        'uses' =>'ServiceController@newTrips',
        'as'   =>'new-loads',
        'title'=>' حمولات جديده ',
        'icon' =>'<i class="fa fa-truck"></i>',
        'hasFather' => true
    ]);
    // reservation trips
    Route::get('reserve-loads',[
        'uses' =>'ServiceController@reserveLoads',
        'as'   =>'reserve-loads',
        'title'=>' حمولات محجوزه ',
        'icon' =>'<i class="fa fa-truck"></i>',
        'hasFather' => true
    ]);
    Route::get('old-loads',[
        'uses' =>'ServiceController@oldloads',
        'as'   =>'old-loads',
        'title'=>' حمولات قديمه ',
        'icon' =>'<i class="fa fa-truck"></i>',
        'hasFather' => true
    ]);
    // show trips
    Route::get('show-loads/{id}',[
        'uses' =>'ServiceController@showload',
        'as'   =>'show-loads',
        'title'=>' عرض حموله ',
    ]);

    /*------------ End Of ServiceController ----------*/
    /*------------ start Of ReservationController ----------*/
    Route::get('reservations',[
        'uses' =>'ReservationController@reservations',
        'as'   =>'reservations',
        'title'=>'قائمة الحجوزات',
        'subTitle'=> ' الحوجزات الحالية ',
        'icon' =>'<i class="glyphicon glyphicon-shopping-cart"></i>',
        'subIcon' =>'<i class="fa fa-cart-plus"></i>',
        'child'=>[
            'finsh-reservations',

        ]
    ]);
    // finsh reservation
    Route::get('finsh-reservations',[
        'uses' =>'ReservationController@reseriveration_finsh',
        'as'   =>'finsh-reservations',
        'title'=>' حجوزات منتهيه ',
        'icon' =>'<i class="fa fa-truck"></i>',
        'hasFather' => true
    ]);

    /*------------ end Of ServiceController ----------*/

    /*------------ Start Of ComplationController ----------*/

    #messages page
    Route::get('complation-page',[
        'uses' =>'ComplationController@InboxPage',
        'as'   =>'complation',
        'title'=>'الشكاوى والمقترحات',
        'icon' =>'<i class="icon-inbox-alt"></i>',
        'child' =>['showComplation','deleteComplation','sendsmsComplation','sendemailComplation']
    ]);

    #show message page
    Route::get('show-complation/{id}',[
        'uses'=>'ComplationController@ShowMessage',
        'as'  =>'showComplation',
        'title'=>'عرض '
    ]);

    #send sms
    Route::post('send-sms-complation',[
        'uses' =>'ComplationController@SMS',
        'as'   =>'sendsmsComplation',
        'title'=>'ارسال SMS'
    ]);

    #send email
    Route::post('send-email-complation',[
        'uses' =>'ComplationController@EMAIL',
        'as'   =>'sendemailComplation',
        'title'=>'ارسال Email'
    ]);

    #delete message
    Route::post('delete-message-complation',[
        'uses' =>'ComplationController@DeleteMessage',
        'as'   =>'deleteComplation',
        'title'=>'حذف الشكوى'
    ]);

    /*------------ End Of ComplationController ----------*/


    /*------------ Start Of MoneyAccountsController ----------*/
    Route::get('money-accounts',[
        'uses' =>'MoneyAccountsController@MoneyAccountsPage',
        'as'   =>'moneyaccountspage',
        'icon' =>'<i class="icon-cash3"></i>',
        'title'=>'الحسابات الماليه',
        'child'=>['moneyaccept','moneyacceptdelete','moneydelete']
    ]);

    #accept
    Route::post('accept',[
        'uses' =>'MoneyAccountsController@Accept',
        'as'   =>'moneyaccept',
        'title'=>'تأكيد معامله بنكيه',
    ]);

    #accept and delete
    Route::post('accept-delete',[
        'uses' =>'MoneyAccountsController@AcceptAndDelete',
        'as'   =>'moneyacceptdelete',
        'title'=>'تأكيد مع حذف',
    ]);

    #delete
    Route::post('money-delete',[
        'uses' =>'MoneyAccountsController@Delete',
        'as'   =>'moneydelete',
        'title'=>'حذف معامله بنكيه',
    ]);
    /*------------ End Of MoneyAccountsController ----------*/
    /*------------ Start Of ReportsController ----------*/

    #reports page
    Route::get('reports-page',[
        'uses' =>'ReportsController@ReportsPage',
        'as'   =>'reportspage',
        'title'=>'التقارير',
        'icon' =>'<i class=" icon-flag7"></i>',
        'child'=>['deleteusersreports','deletesupervisorsreports']
    ]);

    #delete users reports
    Route::post('delete-users-reporst',[
        'uses' =>'ReportsController@DeleteUsersReports',
        'as'   =>'deleteusersreports',
        'title'=>'حذف تقارير الاعضاء'
    ]);

    #delete supervisors reports
    Route::post('delete-supervisors-reporst',[
        'uses' =>'ReportsController@DeleteSupervisorsReports',
        'as'   =>'deletesupervisorsreports',
        'title'=>'حذف تقارير المشرفين'
    ]);
    /*------------ End Of ReportsController ----------*/


});
//	Route::get('dd',function(){
//		 echo bcrypt(123456);
//	});
/*-------------------------------End Of DashBoard--------------------------------*/



//Login Route
Route::get('/login/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login/', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

// Route::get('register', 'Auth\RegisterController@showRegistrationForm');
// Route::post('register','RegisterUserController@Register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
