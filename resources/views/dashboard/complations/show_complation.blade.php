@extends('dashboard.layout.master')

<!-- style -->
@section('style')

@endsection
<!-- /style -->
@section('title','عرض رساله او شكوى '.$message->User->name)
@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">عرض رساله :{{$message->User->name}}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row text-center">
                <div class="col-sm-12 alert alert-success">
                    <div class="col-sm-3">اسم الراسل : {{$message->User->name}} </div>
                    <div class="col-sm-3">البريد : {{$message->User->email}}</div>
                    <div class="col-sm-3">الهاتف : {{$message->User->phone}}</div>
                    <div class="col-sm-3">التاريخ : {{$message->created_at->diffForHumans()}}</div>
                </div>

                <br>
                <div class="col-sm-12" style="margin-top: 20px;margin-bottom: 25px">
                    {{$message->msg}}
                </div>
                <div class="col-sm-12" style="margin-top: 20px;margin-bottom: 25px">
                  <h2 class="h1">اضافات</h2>
                        <div class="row">
                            @forelse ($message->ComplationImages as $image)
                                <div class="col-sm-4">
                                    <img class="img-responsive" src="{{asset('dashboard/uploads/addations/'.$image->addation)}}" alt="">
                                </div>
                            @empty
                                <p>لايوجد اضافات</p>
                            @endforelse
                        </div>
                </div>
                <div class="col-sm-12" style="margin-top:20px" >
                    <div class="btn btn-danger col-sm-3" id="delete">حذف <i style="color: #fff" class=" icon-trash"></i> </div>

                    <div class="btn btn-primary col-sm-3 SMS"
                         data-toggle="modal"
                         data-target="#exampleModalSMS"
                         data-phone="{{$message->User->phone}}"
                         data-email="{{$message->User->email}}"
                         data-name="{{$message->User->name}}">
                        رد برساله SMS <i class="icon-mobile2"></i>
                    </div>

                    <div class="btn btn-success col-sm-3 EMAIL"
                         data-toggle="modal"
                         data-target="#exampleModalEmail"
                         data-phone="{{$message->User->phone}}"
                         data-email="{{$message->User->email}}"
                         data-name="{{$message->User->name}}">
                        رد برساله Email <i class="icon-mail5"></i>
                    </div>

                    <div class="btn btn-warning col-sm-3"><a style="color: #fff" href="{{route('complation')}}">عوده لصندوق الوارد <i class="icon-enter5"></i> </a></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SMS Modal -->
<div class="modal fade" id="exampleModalSMS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ارسال رساله SMS لـ<span class="reverName"></span></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="{{route('sendsmsComplation')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="phone" value="">
                        <div class="col-sm-12">
                            <textarea rows="15" name="sms_message" class="form-control" placeholder="نص الرساله "></textarea>
                        </div>

                        <div class="col-sm-12" style="margin-top: 10px">
                            <button type="submit" class="btn btn-primary addCategory">ارسال</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /SMS Modal -->
{{--form delete--}}
<form action="{{route('deleteComplation')}}" method="POST" style="display: none" id="formdelte">
    {{csrf_field()}}
    <input type="hidden" name="id" value="{{$message->id}}">
    <button type="submit" class=" btn btn-danger btn-block" title="حذف"><i class="icon-trash"></i></button>
</form>
<!-- Email Modal -->
<div class="modal fade" id="exampleModalEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ارسال رساله Email لـ<span class="reverName"></span></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="{{route('sendemailComplation')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="email">
                        <input type="hidden" name="name">
                        <div class="col-sm-12">
                            <textarea rows="15" name="email_message" class="form-control" placeholder="نص الرساله "></textarea>
                        </div>

                        <div class="col-sm-12" style="margin-top: 10px">
                            <button type="submit" class="btn btn-primary addCategory">ارسال</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /Email Modal -->

<!-- javascript -->
@section('script')
<script>
    //put phone in the modal
    $(document).on('click','.SMS',function(){
        $('input[name="phone"]').val($(this).data('phone'));
        $('.reverName').text($(this).data('name'))
    });
    $(document).on("click","#delete",function () {
        $("#formdelte").submit();

    });
    //put email in the modal
    $(document).on('click','.EMAIL',function(){
        $('input[name="email"]').val($(this).data('email'));
        $('input[name="name"]').val($(this).data('name'));
        $('.reverName').text($(this).data('name'))
    });
</script>
@endsection


@endsection