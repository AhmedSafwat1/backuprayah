
@extends('dashboard.layout.master')

<!-- style -->
@section('style')
    <style type="text/css">
        .modal .icon-camera
        {
            font-size: 100px;
            color: #797979
        }

        .modal input
        {
            margin-bottom: 4px
        }

        .reset
        {
            border:none;
            background: #fff;
            margin-right: 11px;
        }

        .icon-trash
        {
            margin-left: 8px;
            color: red;
        }

        .dropdown-menu
        {
            min-width: 88px;
        }

        #hidden
        {
            display: none;
        }
    </style>
@endsection
<!-- /style -->
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">قائمة  {{$name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <!-- <li><a data-action="close"></a></li> -->
                </ul>
            </div>
        </div>

        <!-- buttons -->
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6">
                    <button class="btn bg-purple-300 btn-block btn-float btn-float-lg" type="button"><i class="glyphicon glyphicon-shopping-cart"></i> <span>عدد   {{$name}} : {{count($reserveation)}} </span> </button></div>
                <div class="col-xs-6">
                    <a href="{{route('logout')}}" class="btn bg-warning-400 btn-block btn-float btn-float-lg" type="button"><i class="icon-switch"></i> <span>خروج</span></a>
                </div>
            </div>
        </div>
        <!-- /buttons -->

        <table class="table datatable-basic">
            <thead>
            <tr>
                <th> رقم الحجز </th>
                <th> أسم طالب الحجز </th>
                <th>  رقم الجوال </th>
                <th> الاسم </th>
                <th>ثمن  الخدمه </th>
                <th>صاحب الخدمه </th>
                <th> الحاله</th>
                <th>التفاصيل</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reserveation as $u)
                <tr>
                    <td>{{$u->id}}</td>
                    <td>{{$u->User->name}}</td>
                    <td>{{$u->phone}}</td>
                    <td>{{$u->name}}</td>
                    <td>{{$u->Service->price}} ريال </td>
                    <td>{{$u->Service->User->name}}</td>
                    <td>
                        @if($u->status == 0)
                            حالي
                        @elseif($u->status == 1)
                            منتهى
                        @else
                            مرفوض
                        @endif
                    </td>

                    <td>
                        @if($u->type==2)
                            <a href="{{route("show-loads",$u->id)}}">
                                <button type="button" class="btn btn-primary"> عرض</button>
                            </a>
                        @else
                            <a href="{{route("show-trips",$u->id)}}">
                                <button type="button" class="btn btn-primary"> عرض</button>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

    <!-- javascript -->
@section('script')
    <script type="text/javascript" src="{{asset('dashboard/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashboard/js/pages/datatables_basic.js')}}"></script>
@endsection

@endsection