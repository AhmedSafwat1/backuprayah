@extends('dashboard.layout.master')

<!-- style -->
@section('style')
    <link href="{{asset('dashboard/fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dashboard/fileinput/css/fileinput-rtl.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dashboard/bgrins/spectrum.css')}}" rel='stylesheet' >

    <style>
        .tab-content > .tab-pane {
            display: block;
        }
    </style>

@endsection
<!-- /style -->
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title"> رقم {{$name}} {{ $trip->id }} </h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li><a href="#basic-tab2" data-toggle="tab"> تفاصيل {{$name}} </a></li>

                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane" id="basic-tab2">
                                <div class="col-md-12">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> بيانات العميل عارض {{$name}} </h5>
                                        </div>

                                        <div class="panel-body">

                                            <table class="table datatable-basic">
                                                <thead>
                                                <tr>
                                                    <th> رقم {{$name}}  </th>
                                                    <th> الاسم </th>
                                                    <th> رقم الجوال</th>
                                                    <th> البريد الالكترونى </th>
                                                    <th> العنوان </th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{$trip->id}}</td>
                                                    <td> {{$trip->User->name}}</td>
                                                    <td>{{$trip->User->phone}}</td>
                                                    <td>{{$trip->User->email}}</td>
                                                    <td>{{$trip->User->address}}</td>


                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> خط سير {{{$name}}}  </h5>
                                        </div>

                                        <div class="panel-body">
                                            <table class="table datatable-basic">

                                                <tbody>
                                                <tr>
                                                    <td>من </td>
                                                    <td> {{$trip->from_address}}</td>
                                                </tr>
                                                <tr>
                                                    <td>الى </td>
                                                    <td> {{$trip->to_address}}</td>
                                                </tr>
                                                @if($flag == 2)
                                                    <tr>
                                                        <td> نوع الحموله </td>
                                                        <td> {{$trip->load_type}}</td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            <hr>
                                            <table class="table datatable-basic">
                                                <thead>
                                                    <td>مدة {{$name}}</td>
                                                    <td>سعر {{$name}}</td>
                                                    <td>تاريخ {{$name}}</td>
                                                    <td>وقت {{$name}}</td>
                                                    <td>حالة {{$name}}</td>
                                                    <td>تاريخ الاضافه</td>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{$trip->duration}} دقيقه </td>
                                                    <td>{{$trip->price}}ريال </td>
                                                    <td>{{$trip->date}}</td>
                                                    <td>{{\Carbon\Carbon::parse($trip->time)->format("g:i A")}}</td>
                                                    <td>
                                                        @if($trip->status == 0)
                                                            قديمه
                                                        @elseif($trip->status == 1)
                                                            متاحه
                                                        @elseif($trip->status == 2)
                                                            محجوزه
                                                        @else
                                                            مرفوض
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{$trip->created_at->diffForHumans()}}
                                                    </td>
                                                </tr>

                                                </tbody>
                                            </table>
                                            <table class="table datatable-basic">
                                                <div id="map2" style="width:100%; height:250px;"></div>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--==================== -->
                                @if($reserivation)
                                    <div class="col-md-12">
                                        <div class="panel panel-flat">
                                            <div class="panel-heading">
                                                <h5 class="panel-title"> تفاصيل الحجز </h5>
                                            </div>

                                            <div class="panel-body">

                                                <table class="table datatable-basic">
                                                    <thead>
                                                    <td>صاحب الحجز</td>
                                                    <td>رقم الجوال</td>
                                                    <td>العنوان</td>
                                                    <td>تاريخ الحجز</td>
                                                    </thead>
                                                    <tbody>
                                                    <td>{{$reserivation->User->name}}</td>
                                                    <td>{{$reserivation->User->phone}}</td>
                                                    <td>{{$reserivation->User->address}}</td>
                                                    <td>{{$reserivation->User->created_at->format("d-m-Y")}}</td>
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>
                                    </div>
                                @endif
                            <!--==================== -->
                                <div class="col-md-12">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"> مكان صحاب الخدمه على الخريطة  </h5>
                                            <h6> تفاصيل العنوان  : {{$trip->User['address']}} </h6>
                                        </div>

                                        <div class="panel-body">

                                            <table class="table datatable-basic">
                                                <div id="map" style="width:100%; height:250px;"></div>
                                            </table>

                                        </div>
{{--                                        ==============--}}




                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

        <!-- javascript -->
        @section('script')
            <script src="{{asset('dashboard/bgrins/spectrum.js')}}"></script>

            <script type="text/javascript">
                //stay in current tab after reload
                $(function() {
                    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
                    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                        // save the latest tab; use cookies if you like 'em better:
                        localStorage.setItem('lastTab', $(this).attr('href'));
                    });

                    // go to the latest tab, if it exists:
                    var lastTab = localStorage.getItem('lastTab');
                    if (lastTab) {
                        $('[href="' + lastTab + '"]').tab('show');
                    }
                });


                $(document).on('change','#color',function(){
                    console.log($(this).val());
                });
            </script>


    @endsection
    <!-- /javascript -->

        <script>

            // testt
            function initMap() {

                var map = new google.maps.Map(document.getElementById('map2'), {
                    zoom: 12    ,
                    center: {lat: parseFloat({{$trip->to_lat}}), lng: parseFloat({{$trip->to_lng}})},
                    mapTypeId: 'terrain'
                });

                var lineSymbol = {
                    path: 'M 0,-1 0,1',
                    strokeOpacity: 1,
                    scale: 4
                };
                var flightPlanCoordinates = [
                    {lat: parseFloat({{$trip->from_lat}}), lng: parseFloat({{$trip->from_lng}})},
                    {lat: parseFloat({{$trip->to_lat}}), lng: parseFloat({{$trip->to_lng}})},
                ];
                var marker = new google.maps.Marker({
                    position: flightPlanCoordinates[0],
                    map: map,
                    title: "من "+"{{$trip->from_address}}"
                });
                var marker2 = new google.maps.Marker({
                    position: flightPlanCoordinates[1],
                    map: map,

                    title: "الى "+"{{$trip->to_address}}"
                });
                var flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 0,
                    strokeWeight: 2,
                    icons: [{
                        icon: lineSymbol,
                        offset: '0',
                        repeat: '20px'
                    }]
                });

                flightPath.setMap(map);
                // map second for user

                var myLatLng = {lat:parseFloat({{$trip->User->lat}}),lng:parseFloat({{$trip->User->lng}}) };
                var map2 = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: myLatLng
                });
                // console.log(map,myLatLng);
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map2,
                });
            }




        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5uC_mExFIMSehvCgsjegxcF7mTpKmI4w&callback=initMap">
        </script>

        <script type="text/javascript" src="{{asset('dashboard/fileinput/js/fileinput.min.js')}}"></script>
@endsection