<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('first_id')->unsigned();
            $table->bigInteger('second_id')->unsigned();
            $table->bigInteger('service_id')->unsigned();
            $table->integer("first_status")->unsigned()->default(1);
            $table->integer("second_status")->unsigned()->default(1);
            $table->integer('status')->unsigned()->default(0);
            $table->foreign('first_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('second_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_chats');
    }
}
