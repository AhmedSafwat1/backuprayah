<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplationImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complation_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('complation_id')->unsigned();
            $table->string("addation");
            $table->foreign('complation_id')->references('id')->on('complations')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complation_images');
    }
}
