<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string("from_lat")->nullable();
            $table->string("from_lng")->nullable();
            $table->text("from_address")->nullable();
            $table->string("to_lat")->nullable();
            $table->string("to_lng")->nullable();
            $table->text("to_address")->nullable();
            $table->time("time");
            $table->date("date");
            $table->integer("duration")->unsigned()->default(0);
            $table->bigInteger("transport_id")->unsigned();
            $table->integer("price")->unsigned();
            $table->integer("type")->unsigned();
            $table->string("load_type")->nullable();
            $table->integer("status")->unsigned()->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('transport_id')->references('id')->on('transports')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
