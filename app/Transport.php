<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    //Service
    protected $fillable = ['name_ar', 'name_en'];

    public function Services()
    {
        return $this->hasMany('App\Service');
    }
}
