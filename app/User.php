<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','mobile','code','device_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Role()
    {
        return $this->hasOne('App\Role','id','role');
    }

    public function Reports()
    {
        return $this->hasMany('App\Report','user_id','id');
    }
    // message which user as sender start first speak
    public function MessagesSender()
    {
        return $this->hasMany('App\ChatMessage ','sender_id','id');
    }
    // chat room which user as sender start first speak
    public function MessagesReceriver()
    {
        return $this->hasMany('App\ChatMessage ','reciever_id','id');
    }
    //rom created
    public function RoomsCreated()
    {
        return $this->hasMany(RoomChat::class,'first_id','id');
    }
    // rom join
    public function RoomsJoins()
    {
        return $this->hasMany('App\RoomChat','second_id','id');
    }
    // user has many services
    public function Services()
    {
        return $this->hasMany('App\Service');
    }
    //Rate Given
    public function RatesGiven()
    {
        return $this->hasMany('App\Rate',"user_id","id");
    }
    //Rate Owner
    public function RatesOwner()
    {
        return $this->hasMany('App\Rate',"provider_id","id");
    }
    //reservation Owner
    public function Reservations()
    {
        return $this->hasMany('App\Reservation',"user_id","id");
    }
    //complations Owner
    public function Complations()
    {
        return $this->hasMany('App\Complation');
    }
    //device Owner
    public function Devices()
    {
        return $this->hasMany('App\Device');
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }
    public function getJWTCustomClaims() {
        return [];
    }

}
