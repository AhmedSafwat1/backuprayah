<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ChatMessage;
class RoomChat extends Model
{
    //
    // user Sender
    public function RoomCreate()
    {
        return $this->belongsTo('App\User',"first_id" );
    }
    // user reciever
    public function RoomJoin()
    {
        return $this->belongsTo('App\User', "second_id");
    }
    // message in rom
    public function Messages()
    {

        return $this->hasMany(ChatMessage::class,'roomchat_id','id');
    }
}
