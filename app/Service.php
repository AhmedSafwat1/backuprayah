<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'status',
    ];
    // user have
    public function User()
    {
        return $this->belongsTo('App\User');
    }
    // user have
    public function Transport()
    {
        return $this->belongsTo('App\Transport');
    }
    //reservation have
    public function Reservations()
    {
        return $this->hasMany('App\Reservation',"service_id");
    }
    // chat about the  service
    public function RoomChats()
    {
        return $this->hasMany('App\RoomChat ','service_id','id');
    }
}
