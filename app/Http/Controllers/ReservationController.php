<?php

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    //
    // ======================== start trips ===============
    public function reservations(Request $request)
    {
        $reserveation = Reservation::whereStatus(0)->get();
        $name =  "  الحجوزات الحاليه ";
        return view('dashboard.reservations.reservations',compact('reserveation',"name"));
    }
    public function reseriveration_finsh(Request $request)
    {
        $reserveation = Reservation::whereStatus(1)->get();
        $name =  "  الحجوزات المنتهيه ";
        return view('dashboard.reservations.reservations',compact('reserveation',"name"));
    }
}
