<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use function PHPSTORM_META\type;

class ServiceController extends Controller
{
    // ======================== start trips ===============
    public function trips(Request $request)
    {
        $trips = Service::whereType(1)->whereStatus(1)->get();
        $name =  " الرحلات الحاليه";
        $flag = 1;
        return view('dashboard.services.services',compact('trips',"name","flag"));
    }
    // public function oldTrips
    public function oldTrips(Request $request)
    {
        $trips = Service::whereType(1)->whereStatus(0)->get();
        $name =  " الرحلات القديمه";
        $flag = 1;
        return view('dashboard.services.services',compact('trips',"name","flag"));
    }
    // public function oldTrips
    public function reserveTrips(Request $request)
    {
        $trips = Service::whereType(1)->whereStatus(2)->get();
        $name =  " الرحلات المحجوزه";
        $flag = 1;
        return view('dashboard.services.services',compact('trips',"name","flag"));
    }
    //show trip
    public function showtrip(Request $request , $id)
    {
        $trip = Service::findOrFail($id);
        $flag = $trip->type;
        $reserivation  = $trip->Reservations()->first();
        $name = $flag == 1 ? "الرحله":"الحموله";
        return view('dashboard.services.details',compact('trip','flag','name','reserivation'));
    }
    //end trips==================
    public function loads(Request $request)
    {
        $trips = Service::whereType(2)->whereStatus(1)->get();
        $name =  " الحمولات الحاليه";
        $flag = 2;
        return view('dashboard.services.services',compact('trips',"name","flag"));
    }
    // public function oldTrips
    public function oldloads(Request $request)
    {
        $trips = Service::whereType(2)->whereStatus(0)->get();
        $name =  " الحمولات القديمه";
        $flag = 2;
        return view('dashboard.services.services',compact('trips',"name","flag"));
    }
    // public function oldTrips
    public function reserveLoads(Request $request)
    {
        $trips = Service::whereType(2)->whereStatus(2)->get();
        $name =  " الحمولات المحجوزه";
        $flag = 2;
        return view('dashboard.services.services',compact('trips',"name","flag"));
    }
    //show trip
    public function showload(Request $request , $id)
    {
        $trip = Service::findOrFail($id);
        $flag = $trip->type;
        $reserivation  = $trip->Reservations()->first();
        $name = $flag == 1 ? "الرحله":"الحموله";
        return view('dashboard.services.details',compact('trip','flag','name','reserivation'));
    }
}
