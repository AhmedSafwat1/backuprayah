<?php

namespace App\Http\Controllers;

use App\Complation;
use App\Contact;
use Session;
use App\Mail\MailMessage;
use App\SmsEmailNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ComplationController extends Controller
{
    //
    #complation page
    public function InboxPage()
    {
        $messages = Complation::latest()->get();
        return view('dashboard.complations.complations',compact('messages',$messages));
    }
    #show message
    public function ShowMessage($id)
    {

        $message = Complation::findOrFail($id);
        $message->status = 1;
        $message->update();
        return view('dashboard.complations.show_complation',compact('message',$message));
    }

    #send SMS
    public function SMS(Request $request)
    {
        $this->validate($request,[
            'phone'       =>'required',
            'sms_message' =>'required|min:1'
        ]);

        if(send_mobile_sms($request->phone,$request->sms_message))
        {
            Session::flash('success','تم ارسال الرساله');
            return back();
        }else
        {
            Session::flash('warning','لم يتم ارسال الرساله ! ... تأكد من بيانات ال SMTP');
            return back();
        }
    }

    #send EMAIL
    public function EMAIL(Request $request)
    {
        $this->validate($request,[
            'email'=>'required',
            'email_message' =>'required|min:1'
        ]);

        #check if smtp congiration complete or no
        $checkConfig = SmsEmailNotification::first();
        if(
            $checkConfig->smtp_type         == "" ||
            $checkConfig->smtp_username     == "" ||
            $checkConfig->smtp_password     == "" ||
            $checkConfig->smtp_sender_email == "" ||
            $checkConfig->smtp_port         == "" ||
            $checkConfig->smtp_host         == ""
        ){
            Session::flash('danger','لم يتم ارسال الرساله ! .. يرجى مراجعة بيانات ال SMTP');
            return back();
        }else
        {
            Mail::to($request->email)->send(new MailMessage($request->email_message));
            Session::flash('success','تم ارسال الرساله');
            return back();
        }
    }

    #delete mesage
    public function DeleteMessage(Request $request)
    {
        $comp = Complation::findOrFail($request->id)->delete();
        if(count($comp->ComplationImages) > 0)
        {
            foreach ($comp->ComplationImages as $image)
            {
                File::delete('dashboard/uploads/addations/'.$image->addation);
            }
        }
        Session::flash('success','تم حذف الرساله');
        return back();
    }
}
