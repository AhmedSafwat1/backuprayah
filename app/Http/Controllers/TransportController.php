<?php

namespace App\Http\Controllers;

use App\Transport;
use Illuminate\Http\Request;
use Auth;
use Session;
class TransportController extends Controller
{
    //
    //
    public function  __construct()
    {
        \Carbon\Carbon::setLocale('ar');
    }
    // display transports
    public function transports ()
    {

        $transports   = Transport::all();
        return view('dashboard.transports.transports ',compact('transports'));
    }
    // add Transport
    public function add(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|min:3|max:190|unique:transports,name_ar',
            'name_en' => 'required|min:3|max:190|unique:transports,name_en',

        ]);
        Transport::create($request->all());
        Report(Auth::user()->id,'بأضافة وسيله جديد');
        Session::flash('success','تم اضافة وسيله');
        return back();
    }
    // delete Transport
    public function delete(Request $request)
    {
        // dd($request->all());
        $transport = Transport::findOrFail($request->id);
        $transport->delete() ;
        Report(Auth::user()->id,'بحذف وسيلة النقل '.$transport->name);
        Session::flash('success','تم الحذف');
        return back();
    }
    public function edit(Request $request)
    {
        $transport = Transport::findOrFail($request->id);
        $this->validate($request, [
            'edit_name_ar' => 'required|min:3|max:190|unique:transports,name_ar,'.$transport["id"],
            'edit_name_en' => 'required|min:3|max:190|unique:transports,name_ar,'.$transport["id"],
        ]);
        $transport->name_ar = $request->edit_name_ar;
        $transport->name_en = $request->edit_name_en;
        $transport->save();
        AdminReport(Auth::user()->id,'قام بتعديل وسيلة النقل '.$transport->name_ar);
        Session::flash('success','تم تعديل');
        return back();
    }
}
