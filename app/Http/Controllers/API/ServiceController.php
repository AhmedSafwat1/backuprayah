<?php

namespace App\Http\Controllers\API;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\TimeRight;
use Validator;
use Auth;
use URL;
use Image;

class ServiceController extends Controller
{
    // save the service
    protected function saveService($request, $type)
    {
        $service                = new Service();
        $service->type          = $type;
        $service->status        = 1;
        $service["from_address"]=$request["from_address"];
        $service["to_address"]  =$request["to_address"];
        $service["from_lat"]    =$request["from_lat"];
        $service["from_lng"]    =$request["from_lng"];
        $service["to_lat"]      =$request["to_lat"];
        $service["to_lng"]      =$request["to_lng"];;
        $service["time"]        =$request["time"];
        $service["date"]        =$request["date"];
        $service["price"]       =$request["price"];
        $service["duration"]    =$request["duration"];
        $service["transport_id"]=$request["transport_id"];
        if($type == 2)
            $service["load_type"]   =$request["load_type"];
        $service["user_id"]   =$request["user_id"];
        $service->save();
        return $service;
    }
    // update the service
    protected function updateService($service , $request)
    {
        if(isset($request["from_address"]) && !empty($request["from_address"]))
        {
            $service["from_address"]=$request["from_address"];
            $service["from_lat"]    =$request["from_lat"];
            $service["from_lng"]    =$request["from_lng"];
        }
        if(isset($request["to_address"]) && !empty($request["to_address"]))
        {
            $service["to_address"]  =$request["to_address"];
            $service["to_lat"]      =$request["to_lat"];
            $service["to_lng"]      =$request["to_lng"];
        }
       if(isset($service["date"] )&& !empty($service["date"] ))
           $service["date"]         =$request["date"];
       if(isset($service["price"])&& !empty($service["price"]))
           $service["price"]       =$request["price"];
       if(isset($service["price"] )&& !empty($service["price"]))
           $service["price"]       =$request["price"];
       if(isset( $service["duration"])&& !empty( $service["duration"]))
           $service["duration"]    =$request["duration"];
       if(isset( $service["duration"])&& !empty( $service["duration"]))
            $service["transport_id"]=$request["transport_id"];
       if($service["type"] == 2 && isset($service["load_type"])&& !empty( $service["load_type"]))
            $service["load_type"]   =$request["load_type"];
       $service->update();
       return $service;
    }
    // reponse the service
    protected  function responseServie($service,$lang,$single=1)
    {
        $res["id"]           = $service["id"];
        $res["type"]         = $service["type"];
        $res["status"]       = $service["status"];
        $res["from_address"] = $service["from_address"];
        $res["from_lat"]     = $service["from_lat"];
        $res["from_lng"]     = $service["from_lng"];
        $res["time"]         = $service["to_lat"];
        $res["date"]         = $service["to_lng"];
        $res["price"]        = $service["id"];
        $res["duration"]     = $service["id"];
        if($service["type"] == "2")
            $res["load_type"] = $service["load_type"];
        $res["transport_id"] =$service["transport_id"];
        $name                = $lang == "ar"?$service->Transport->name_ar:$service->Transport->name_en;
        $res["transport_name"]=$name;
        if($single==1)
        {
            $user                = $service->User;
            $dataUser["id"]      = $user->id;
            $dataUser["name"]    = $user->name;
            $dataUser["avatar"]  = URL::to('dashboard/uploads/users') . '/' . $user['avatar'];
            $dataUser["rate"]    = getUserRate($user);
            $res["user"]         = $dataUser;
        }
        return $res;
    }
    //response pagantion
    protected  function responseServiePagnaion($page,$lang)
    {
        $service               = getCollectionPagantion($page);
        $res["data"]           = $service->map(function ($serv) use($lang){
            return $this->responseServie($serv, $lang, 0);
        });
        $res["meta"]           = getBasicInfoPagantion($page);
        return $res;
    }
    /******************* start trips   *********************/
    // add trip
    public function addTrip(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'from_address'   =>'required',
            'from_lat'       =>'required',
            'from_lng'       =>'required',
            'to_address'     => 'required',
            'to_lat'         =>'required',
            'to_lng'         => 'required',
            'time'           => ['required',new TimeRight($request["lang"])],
            'date'           => 'required|after_or_equal:today',
            'duration'       => 'required|integer|min:0',
            'transport_id'   => 'required|exists:transports,id',
            'price'          => 'required|integer|min:0',
            'user_id'        => 'required|exists:users,id',
        ]);

        if ($validator->passes())
        {
            $this->saveService($request, 1);
            AdminReport($request["user_id"],'قام باضافه رحله جديده');
            $msg = $request["lang"]=="ar"?"تم  اضافه الرحله  بنجاح":"sucessfull add the triple ";
            return response()->json(['key'=>'success','value'=>'1','data'=>$msg]);

        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    /******************* end trips     *********************/
    /******************* start loads   *********************/
    // add load
    public function addLoad(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'from_address'   =>'required',
            'from_lat'       =>'required',
            'from_lng'       =>'required',
            'to_address'     => 'required',
            'to_lat'         =>'required',
            'to_lng'         => 'required',
            'time'           => ['required',new TimeRight($request["lang"])],
            'date'           => 'required|after_or_equal:today',
            'duration'       => 'required|integer|min:0',
            'transport_id'   => 'required|exists:transports,id',
            'price'          => 'required|integer|min:0',
            'load_type'      =>'required',
            'user_id'        => 'required|exists:users,id',
        ]);

        if ($validator->passes())
        {
            $this->saveService($request, 2);
            AdminReport($request["user_id"],'قام باضافه حموله جديده');
            $msg = $request["lang"]=="ar"?"تم  اضافه الحموله  بنجاح":"sucessfull add the load ";
            return response()->json(['key'=>'success','value'=>'1','data'=>$msg]);

        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }

    /******************* end loads     *********************/
    //update service
    public function editService(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'service_id'     =>"required|exists:services,id",
            'from_address'   =>'nullable',
            'from_lat'       =>'nullable|required_with:from_address',
            'from_lng'       =>'nullable|required_with:from_address',
            'to_address'     => 'nullable',
            'to_lat'         =>'nullable|required_with:to_address',
            'to_lng'         => 'nullable|required_with:to_address',
            'time'           => ['nullable',new TimeRight($request["lang"])],
            'date'           => 'nullable|after_or_equal:today',
            'duration'       => 'nullable|integer|min:0',
            'transport_id'   => 'nullable|exists:transports,id',
            'price'          => 'nullable|integer|min:0',
            'load_type'      => 'nullable',
            'user_id'        => 'required|exists:users,id',
        ]);

        if ($validator->passes())
        {
            $service = Service::where('id',$request["service_id"])
                ->where("user_id",$request["user_id"])
                ->where("status",1)
                ->first();
            if($service)
            {
                $msg = "";
                $key = CheckExpireService($service);
                if($key)
                {
                    $msg = $request["lang"]=="ar"?"تم حفظ التعديلات":"sucessfull update";
                    $this->updateService($service,$request);
                    AdminReport($request["user_id"],"قام بتعديل خدمه",$service->to_address);
                }
                else
                {
                    $msg = $request["lang"]=="ar"?"الخدمه انتهت":"service finish";
                }
                return response()->json(['key'=>'success','value'=>$key,'data'=>$msg]);

            }else
            {
                $msg = $request["lang"]=="ar"?"المستخدم لايملك هذه الخدمه":"sucessfull add the load ";
                return response()->json(['key'=>'success','value'=>'1','data'=>$msg]);
            }
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //deleter serservice
    public function deleteService(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'service_id'     =>"required|exists:services,id",
            'user_id'        => 'required|exists:users,id',
        ]);

        if ($validator->passes())
        {
            $service = Service::where('id',$request["service_id"])
                ->where("user_id",$request["service_id"])
                ->delete();
            if($service)
            {
                $msg = $request["lang"]=="ar"?"تم   حذف الخدمه":"service delete";
                AdminReport($request["user_id"],"قام بحذف خدمه");
                return response()->json(['key'=>'success','value'=>"1",'data'=>$msg]);

            }else
            {
                $msg = $request["lang"]=="ar"?"المستخدم لايملك هذه الخدمه":"sucessfull add the load ";
                return response()->json(['key'=>'success','value'=>'1','data'=>$msg]);
            }
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // get service
    public function getService(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'service_id'     =>"required|exists:services,id",
        ]);

        if ($validator->passes()) {
            $service = Service::find($request["service_id"]);
            $arr     = $this->responseServie($service, $request["lang"]);
            return response()->json(['key' => 'success', 'value' => '1', 'data' =>$arr, 'msg' => '']);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // get services
    // based the status and type: 1 =>trips,  2 =>loads  3=>all servic availabel,status[0=>refuse,1=>not expire ,2 => expire and finsh
    public function getServices(Request $request)
    {
        $validator = Validator::make($request->all(),[
            "type"           =>"required|in:1,2,3",
            "status"         =>"required|in:0,1,2"
        ]);

        if ($validator->passes()) {
            $service = null;
            if($request["type"] != 3)
            {
                $service = Service::where("type",$request["type"])
                    ->where("status",$request["status"])
//                    ->paginate(15);
                    ->get();

            }
            else
            {
                $service = Service::where("status","1")
//                    ->paginate(15);
                     ->get();

            }
            $lang    = $request["lang"];
            $arr     = $service->map(function ($serv) use($lang){
                return $this->responseServie($serv, $lang, 0);
            });
            //paginatiate
//            $arr     = $this->responseServiePagnaion($service,$lang);
            return response()->json(['key' => 'success', 'value' => '1', 'data' =>$arr, 'msg' => '']);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }

}
