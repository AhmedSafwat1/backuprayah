<?php

namespace App\Http\Controllers\API;

use App\Complation;
use App\Contact;
use App\Device;
use App\Rate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Validator;
use Auth;
use URL;
use Image;
use JWTFactory;
use JWTAuth;
class UsersController extends Controller
{
    //   function handle data return from object user
    protected function responseUser($user)
    {
        $res            = [];
        if($user)
        {
            $image = is_null($user['avatar']) ? "default.png" : $user['avatar'];
            $res["id"]      = $user->id;
            $res["name"]    = $user->name;
            $res["phone"]   = $user->phone;
            $res["avatar"]  = URL::to('dashboard/uploads/users') . '/' . $image;
            $res['confirm'] = $user['confirm'];
            $res['active']  = $user['active'];
            $res["rate"]    = getUserRate($user);
        }
        return $res;
    }
    // save device
    protected function SaveDevice($user, $device_type , $device_id)
    {
        $data["device_id"]  = $device_id;
        $data["device_type"]= $device_type;
        $data["user_id"]    = $user->id;
        $user->Devices()->create($data);
    }
    // user Rate
    protected  function reponseRate($user ,$lang)
    {
        $rats = $user->RatesOwner->map(function ($rate)  use($lang){
            $res["id"]          = $rate["id"];
            $res["rate"]        = $rate["rate"];
            $res["comment"]     = is_null($rate["comment"])?"":$rate["comment"];
            $res["created_at"]  = $rate["created_at"]->format('d/m/Y');
            $res["user"]        = collect($rate->User->toArray())->only("name","last","id")->all();
            return $res;
        });
        return $rats;
    }
    // sign up
    public function signUp(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name'       =>'required|min:2|max:190',
            'email'      =>'nullable|email|min:2|unique:users,email',
            'phone'      =>'required|digits_between:5,40|unique:users,phone',
            'password'   =>'required|min:6|max:190',
            'address'    => 'required',
            'lng'        => 'required',
            'lat'        => 'required',
            'device_id'  => 'required',
            'device_type'=> 'required',
        ]);
        if ($validator->passes())
        {
            $user               = new User;
            $user["name"]       = $request["name"];
            $user["phone"]      = $request["phone"];
            $user["password"]   = bcrypt($request['password']);
            $user['lat']        = $request['lat'];
            $user['lng']        = $request['lng'];
            $user['address']    = $request['address'];
            $user['active']     = 1;
            $user['confirm']    = 1;
            $user->save();
            $this->SaveDevice($user, $request["device_type"], $request["device_id"]);
            AdminReport($user["id"],'قام بتسجيل بياناته والدخول');
            return response()->json(['key' => 'success', 'value' => '1', 'data' => $this->responseUser($user), 'msg' => '']);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // login in
    public function signIn(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'phone'      => 'required',
            'device_id'  => 'required',
            'device_type'=> 'required',
            'password'   => 'required',
        ]);
        if ($validator->passes()) {
            if(!Auth::attempt( ['phone'=>$request['phone'],'password' =>$request['password']])){
                $msg = $request['lang'] == 'ar' ? 'رقم الجوال أو كلمه المرور غير صحيحه.' : 'The phone number or password is wrong.';
                return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
            }
            $user = User::where('phone',$request['phone'])
                ->first();
            $this->SaveDevice($user, $request["device_type"], $request["device_id"]);
            $arr = $this->responseUser($user);
            if($user->confirm == 0)
            {
                $arr["code"] = $user["code"];
                $msg = $request['lang'] == 'ar' ? 'المستخدم لم يقم بتاكديد رقم الموبيل.' : ' user not verification phone  .';
                return response()->json(['key'=>'success','value'=>'2',"data"=>$arr,'msg'=>$msg]);
            }
            if($user->active == 0)
            {
                $msg = $request['lang'] == 'ar' ? 'المستخدم محظور حاليا.' : ' user is block now   .';
                return response()->json(['key'=>'success','value'=>'3','msg'=>$msg]);
            }
            AdminReport($user->id,'قام بتسجيل الدخول');
            return response()->json(['key'=>'success','value'=>'1','data'=>$arr, 'msg' => '' ]);
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // logout
    public function logout(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'user_id'    =>"required|exists:users,id",
            'device_id'  => 'required',
        ]);

        if ($validator->passes()) {
            $lang    = $request['lang'];
            $device   = Device::where("user_id", $request["user_id"])
                                ->where("device_id", $request["device_id"])
                                ->delete();
            AdminReport($request["user_id"],'قام بتسجيل الخروج');
            $msg = $lang=="ar"?"تم  تسجيل الخروج بنجاح":"sucessfull logout ";
            return response()->json(['key'=>'success','value'=>'1','data'=>$msg]);
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //update info
    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'       => 'nullable|min:2|max:190',
            'email'      => 'nullable|email|min:2|unique:users,email',
            'phone'      => 'nullable|digits_between:5,40|unique:users,phone',
            'user_id'    => 'required|exists:users,id',
            'avatar'     =>'nullable|image',
            'address'    => 'nullable',
            'lng'        => 'nullable|required_with:address',
            'lat'        => 'nullable|required_with:address',
        ]);
        if ($validator->passes())
        {
            $user               = User::find($request["user_id"]);
            if(isset($request["name"]))
                $user["name"]       = $request["name"];
            if(isset($request["phone"]))
                $user["phone"]      = $request["phone"];
            if(isset($request["lat"]))
                $user['lat']        = $request['lat'];
            if(isset($request["lng"]))
                $user['lng']        = $request['lng'];
            if(isset($request["address"]))
                $user['address']    = $request['address'];
            if(isset($request["avatar"]) && !empty($request->avatar))
            {

                $photo=$request->avatar;
                $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
                if($user->avatar != 'default.png')
                {
                    File::delete('dashboard/uploads/users/'.$user->avatar);
                    Image::make($photo)->save('dashboard/uploads/users/'.$name);
                }
                else
                {
                    Image::make($photo)->save('dashboard/uploads/users/'.$name);
                }
                $user->avatar=$name;
            }
            $user->update();
            AdminReport($user->id,'قام بتعديل البيانات الخاصه به');
            return response()->json(['key' => 'success', 'value' => '1', 'data' => $this->responseUser($user), 'msg' => '']);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //get user
    public function getUser(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'       => 'nullable|min:2|max:190',
            'user_id'    => 'required|exists:users,id',
        ]);
        if ($validator->passes())
        {
            $user               = User::find($request["user_id"]);
            $arr                = $this->responseUser($user);
            $arr["rates"]       = $this->reponseRate($user, $request["lang"]);
            $arr["trips_count"] = $user->Services()->where("type",1)->where("status",2)->count();
            $arr["loads_count"]  = $user->Services()->where("type",2)->where("status",2)->count();
            return response()->json(['key' => 'success', 'value' => '1', 'data' =>$arr, 'msg' => '']);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // chnage langue
    public function ChnageLang(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'user_id'   =>"required|exists:users,id"
        ]);

        if ($validator->passes()) {
            $lang    = $request['lang'];
            $user    = User::find($request["user_id"]);
            $user["lang"]=$lang;
            $user->update();
            AdminReport($user->id,'قام تغير اللغه');
            $msg = $lang=="ar"?"تم تغير اللغه بنجاح":"sucessfull change the langue";
            return response()->json(['key'=>'success','value'=>'1','data'=>$msg]);
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //change user password
    public  function changePassword(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'lang'                      => 'required',
            'user_id'                   =>'required|exists:users,id',
            'password'                  =>'required|min:6|max:190',
            'old_password'              =>'required|min:6|max:190',

        ]);

        if ($validator->passes())
        {
            $user = User::find($request['user_id']);
            if(\Hash::check($request['old_password'],$user->password)) {
                $user['password'] = bcrypt($request['password']);
                $user->save();
                $arr['id'] = $user['id'];
                AdminReport($user->id,'قام تغير كلمة السر');
                $msg = $request['lang'] == 'ar' ? 'تم تغير كلمة السر بنجاح ':" password change successfull";
                return response()->json(['key' => 'success', 'value' => '1', 'data' => $arr, 'msg' => $msg]);
            }else
            {
                $msg = $request['lang'] == 'ar' ? 'رقم السرى القديم غير صحيح ':"old password not correct";
                return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
            }
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }

    }
    // add rate
    public function postRate(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'provider_id'   => 'required|exists:users,id',
            'user_id'       => 'required|exists:users,id',
            'rate'          => 'required',
        ]);
        if ($validator->passes()) {
            $rate = Rate::where('user_id',$request['user_id'])->where('provider_id',$request['provider_id'])->orderBy('created_at','desc')->first();
            if($rate){
                $rate['rate'] = $request['rate'];
                if(isset( $request['comment']))  $rate['comment'] = $request['comment'];
                $rate->update();
                $msg =  $request['lang']=='ar' ? 'تم تحديث التقييم بنجاح.' : 'Your rate updated successfully.';
                return response()->json(['key'=>'success', 'value'=>'1', 'msg'=>$msg]);
            }

            $rate                 = new Rate();
            $rate['provider_id']  = $request['provider_id'];
            $rate['user_id']      = $request['user_id'];
            $rate['rate']         = $request['rate'];
            if(isset( $request['comment']))  $rate['comment'] = $request['comment'];
            $rate->save();
            AdminReport( $request['user_id'],'قام بعمل تقيم ل '.$rate->Provider->name);
            $msg =  $request['lang']=='ar' ? 'تم التقييم بنجاح.' : 'This service successfully rated.';
            return response()->json(['key'=>'success', 'value'=>'1', 'msg'=>$msg]);


        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // complation
    public function complation(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'user_id'       => 'required|exists:users,id',
            'title'         => 'required|min:3,max:191',
            "msg"           => "required",
            "addations"     => "nullable|array",
            "addations.*"   => "image"

        ]);
        if ($validator->passes()) {
            $comp           = new \App\Complation;
            $comp->title    = $request["title"];
            $comp->msg      = $request["msg"];
            $comp->user_id  = $request["user_id"];
            $comp->save();
            if(isset($request["addations"]))
            {
                foreach ($request["addations"] as $add)
                {
                    $photo  =$add;
                    $name   =date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
                    Image::make($photo)->save('dashboard/uploads/addations/'.$name);
                    $comp->ComplationImages()->create(["addation"=>$name]);
                }
            }

            AdminReport( $request['user_id'],'قام بعمل شكوى بعنوان '.$comp->title);
            $msg =  $request['lang']=='ar' ? 'تم عمل الشكوى بنجاح فى انتظار المراجعه والرد.' : 'complation successfully sent watit revision and response.';
            return response()->json(['key'=>'success', 'value'=>'1', 'msg'=>$msg]);


        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //contact us
    public function ContactUs(Request $request){

        $validator = Validator::make($request->all(),[

            'name'     =>'required|min:2|max:190',
            'email'    =>'required|email|min:2',
            'msg'      =>'required|min:6|max:190',

        ]);
        if ($validator->passes())
        {
            $contact            = new Contact();
            $contact['name']    = $request['name'];
            $contact['email']   = $request['email'];
            $contact["message"] = $request["msg"];
            $contact->save();
            $msg =  $request['lang'] == 'ar' ? "تم ارسال رسالتك وفى انتظار المراجعه ":"saved the message  and wait reversion and response";
            return response()->json(['key'=>'success','value'=>'1',"msg"=>$msg]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }

    }
    // forget the password
    public function forgotPassword(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'phone'    => 'required|exists:users,phone',
        ]);

        if ($validator->passes()) {
            $user         = User::where('phone',$request['phone'])
                                ->first();
            $code         = mt_rand(1000, 9999);
            $user['code'] = $code;
            AdminReport($user->id,'قام بعملية  استرجاع كلمه المرور');
            $user->save();
            if($request['lang'] == 'ar'){
                $msg = 'كود استرجاع كلمه المرور الخاص بك هو : '.$code;
            }else{
                $msg = 'The verification code is : '.$code;
            }

            send_mobile_sms($request['phone'],$msg);
            $msg = $request["lang"] == "ar" ? "  تم ارسال الكود بنجاح لجوالك":"Sucessful send the code to phone";
            return response()->json(['key'=>'success','value'=>'1','user_id'=>$user['id'],'code'=>$user["code"],"msg"=>$msg]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //reset paassword
        public function resetPassword(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'user_id'   => 'required|exists:users,id',
            'password'  => 'required|min:6',
            'code'      => 'required',
        ]);

        if ($validator->passes()) {

            $user = User::find($request['user_id']);
            if($user['code'] == $request['code']){
                $user['password'] = bcrypt($request['password']);
                $user->update();

                $msg = $request['lang'] == 'ar' ? ' تم تغيير كلمه المرور بنجاح' :'Your password successfully changed.';
                return response()->json(['key'=>'success','value'=>'1',"data"=>$this->responseUser($user),'msg'=>$msg]);
            }else{
                $msg = $request['lang'] == 'ar' ? 'كود التحقق غير صيحيح' :'Verification code is incorrect.';
                return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
            }
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // resend code
    //resend the code
    public function ResendCode(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'user_id'   => 'required|exists:users,id',
        ]);

        if ($validator->passes()) {
            $user         = User::find($request["user_id"]);
            $code         = mt_rand(1000, 9999);
            $user['code'] = $code;
            $user->save();

            if($request['lang'] == 'ar'){
                $msg = 'كود استرجاع كلمه المرور الخاص بك هو : '.$code;
            }else{
                $msg = 'The verification code is : '.$code;
            }

            send_mobile_sms($user['phone'],$msg);
            $msg = $request["lang"] == "ar" ? "  تم اعادة ارسال الكود بنجاح لجوالك":"Sucessful resend the code to phone";
            return response()->json(['key'=>'success','value'=>'1','user_id'=>$user['id'],'code'=>$user["code"],"msg"=>$msg]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
}
