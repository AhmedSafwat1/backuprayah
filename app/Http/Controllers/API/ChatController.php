<?php

namespace App\Http\Controllers\API;

use App\ChatMessage;
use App\RoomChat;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use URL;
class ChatController extends Controller
{
    // get the rom avavilabel
    public function getChatRom($sender_id, $reciver_id, $service_id ,$flag = 1,$status=0)
    {
        $Rom  = RoomChat::where("status",$status)
                        ->where("service_id",$service_id)
                        ->whereIn('first_id',[$sender_id,$reciver_id])
                        ->whereIn("second_id",[$sender_id,$reciver_id])
                        ->latest()->first();
        if(!$Rom && $flag)
        {
            $Rom                = new RoomChat;
            $Rom["first_id"]    = $sender_id;
            $Rom["second_id"]   = $reciver_id;
            $Rom ["service_id"] = $service_id;
            $Rom->save();
        }
        return $Rom;
    }
    //reponse user Min msg
    public function responseUser($user ,$flag=0)
    {
        $res["id"]     = $user->id;
        $res["name"]   = $user->name;
        if($flag)
        {
            $res["avatar"]  = URL::to('dashboard/uploads/users') . '/' . $user["avatar"];
            $res["rate"]    = getUserRate($user);
        }
        return $res;
    }
    // response the mesage =
    public function responseMsg($msg)
    {
        $res["id"]        = $msg["id"];
        $res["message"]   = $msg["message"];
        $res["sender"]    = $this->responseUser($msg->Sender);
        $res["reciever"]  = $this->responseUser($msg->Reciever);
        $res["status"]    = $msg->status;
        return $res;
    }
    // response chat
    public function responseChat($chat ,$user_id,$flag=1)
    {
        $res["id"]        = $chat["id"];
        $msg              = $chat->Messages()
                            ->where("sender_id",$user_id)
                            ->latest()->first();
        $res["last_msg"]  = is_null($msg)?"":$msg->message;
        $roomCreate       =$chat->RoomCreate;
        $romJoin          =$chat->RoomJoin;
        $res["name"]      = $flag == 1 ? $roomCreate->name : $romJoin->name;
        $image            = $flag == 1 ? $roomCreate->avatar:$romJoin->avatar ;
        $res["image"]     = URL::to('dashboard/uploads/users') . '/' . $image;
        $res["created_at"]= $chat->created_at->format("d/m/Y");
        return $res;
    }
    // save message
    public  function saveMessage(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'message'        =>"required",
            'sender_id'      => 'required|exists:users,id',
            'reciever_id'    => 'required|exists:users,id',
            'service_id'     => 'required|exists:services,id',
        ]);
        if ($validator->passes())
        {
            $rom  = $this->getChatRom($request["sender_id"],$request["reciever_id"],$request["service_id"]);
            $data = $request->except("service_id");
            $rom->Messages()->create($data);
            $msg = $request['lang'] == 'ar' ? 'تم ارسال الرساله بنجاح.' : ' sucessfull send message .';
            return response()->json(['key'=>'sucess','value'=>'1','msg'=>$msg]);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // get chat message
    public function getChatMessages(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'sender_id'      => 'required|exists:users,id',
            'reciever_id'    => 'required|exists:users,id',
            'service_id'     => 'required|exists:services,id',
        ]);
        if ($validator->passes())
        {
            $rom  = $this->getChatRom($request["sender_id"],$request["reciever_id"],$request["service_id"]);
            $arr  = $rom->Messages->map(function ($msg){
                return $this->responseMsg($msg);
            });

            return response()->json(['key'=>'sucess','value'=>'1',"data"=>$arr,'msg'=>""]);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // delete chateom
    public function deleteChatRom(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'chat_id'      => 'required|exists:room_chats,id',
            "user_id"      => 'required|exists:users,id'
        ]);
        if ($validator->passes())
        {
            $rom  = RoomChat::find($request["chat_id"]);
            if($rom && ($rom->first_id == $request["user_id"] || $rom->second_id == $request["user_id"]))
            {
                if($rom->first_id == $request["user_id"])
                {
                    $rom->first_status = 0;
                }
                else
                {
                    $rom->second_status = 0;
                }
                $rom->update();
                $msg = $request['lang'] == 'ar' ? 'تم حذف الدردشه بنجاح.' : ' sucessfull delete chat .';
                return response()->json(['key'=>'sucess','value'=>'1','msg'=>$msg]);
            }
            else
            {
                $msg = $request['lang'] == 'ar' ? 'لايوجد دردشه متاحه.' : ' no chat avaialabel .';
                return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
            }

        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // get chatRomms Based the userid
    public function getChatRoms(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_id'      => 'required|exists:users,id',
        ]);
        if ($validator->passes())
        {
            $user     = User::find($request["user_id"]);
            $romFirst = $user->RoomsCreated()->whereStatus(0)->where("first_status",1)->get();
            $romSecond= $user->RoomsJoins()->whereStatus(0)->where("second_status",1)->get();
            $arr1     = $romFirst->map(function ($rom) use($user){
                return $this->responseChat($rom,$user->id,2);
            })->toArray();
            $arr2     = $romSecond->map(function ($rom) use($user){
                return $this->responseChat($rom, $user->id);
            })->toArray();
            $all = array_merge($arr1,$arr2);
            return response()->json(['key'=>'sucess','value'=>'1',"data"=>$all,'msg'=>""]);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // get the message based in chat id
    public function getChatMessagesById(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'chat_id'      => 'required|exists:room_chats,id',
            "user_id"      => 'required|exists:users,id'
        ]);
        if ($validator->passes())
        {
            $rom  = RoomChat::find($request["chat_id"]);
            $arr  = $rom->Messages->map(function ($msg){
                return $this->responseMsg($msg);
            });
            if($rom->first_id==$request["user_id"])
            {
                $arr["id"]    = $rom->second_id;
                $second["name"]  = $rom->RoomJoin->name;
                $second["image"] = URL::to('dashboard/uploads/users') . '/' .$rom->RoomJoin->avatar;
                $arr["speakingTo"]   = $second;
            }
            else
            {
                $first["id"]     = $rom->first_id;
                $first["name"]   = $rom->RoomCreate->name;
                $first["image"]  = URL::to('dashboard/uploads/users') . '/' .$rom->RoomCreate->avatar;
                $arr["speakingTo"]    = $first;
            }
            return response()->json(['key'=>'sucess','value'=>'1',"data"=>$arr,'msg'=>""]);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }

}
