<?php

namespace App\Http\Controllers\API;

use App\SiteSetting;
use App\Transport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BasicInfoController extends Controller
{
    // get trasnpartnet
    public function GetTransports(Request $request)
    {

        $transports = Transport::all();
        $lang       = $request["lang"];
        $arr        = $transports->map(function ($trans) use($lang){
            $res["id"]   = $trans->id;
            $res["name"] = $lang =="ar"? $trans->name_ar: $trans->name_en;
            return $res;
        });

        return response()->json(['key'=>'success','value'=>'1','data'=>$arr]);
    }
    //copy right
    public function GetCopyRight(Request $request)
    {

            $setting = SiteSetting::first();
            $arr     = [];
            $copy    = $request["lang"] == "ar"?$setting->site_copyrigth : $setting->site_copyrigth_en;
            $arr["copy"]        = is_null($copy) ? "" : $copy;
            return response()->json(['key'=>'success','value'=>'1','data'=>$arr]);
    }
    // about us
    public function aboutUs(Request $request)
    {
            $setting = SiteSetting::first();
            $arr     = [];
            $desc    = $request["lang"] == "ar"?$setting->site_description : $setting->site_description_en;
            $arr["description"] = is_null($desc)?" ": $desc;
            return response()->json(['key'=>'success','value'=>'1','data'=>$arr]);
    }
    //
    //get id for latest user rooms messages

}
