<?php

namespace App\Http\Controllers\API;

use App\Reservation;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use URL;
use Image;
class ReservationController extends Controller
{
    //  save Reservation
    public function saveReservation(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'       =>'required|min:2|max:190',
            'phone'      =>'required|digits_between:5,40',
            'user_id'    => 'required|exists:users,id',
            'service_id' => 'required|exists:services,id',
        ]);
        if ($validator->passes())
        {
            $service = Service::find($request["service_id"]);
            if(CheckExpireService($service))
            {
                $resev              = new Reservation;
                $resev["name"]      = $request["name"];
                $resev["phone"]     = $request["phone"];
                $resev["user_id"]   = $request["user_id"];
                $resev["service_id"]= $request["service_id"];
                $resev->save();
                $service->update(["status"=>2]);
                $msg =  $request['lang']=='ar' ? 'تم عمل الطلب بنجاح.' : 'done make  order.';
                return response()->json(['key'=>'success', 'value'=>'1','data'=>array("id"=>$resev->id),'msg'=>$msg]);
                AdminReport($request["user_id"],"قام بعمل طلب من ".$service->User->name);
            }
            else
            {
                $msg = $request["lang"]=="ar"?"الخدمه انتهت":"service finish";
                return response()->json(['key'=>"fail",'value'=>"0",'msg'=>$msg]);
            }

        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // finsh reervation
    public function finsihReservation(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_id'        => 'required|exists:users,id',
            'reservation_id' => 'required|exists:reservations,id',
        ]);
        if ($validator->passes())
        {
            $resev              = Reservation::where("id",$request["reservation_id"])
                                 ->where("user_id",$request["user_id"])
                                 ->where("status",0)->first();
            if($resev)
            {
                $resev->status = 1;
                $resev->update();
                $msg =  $request['lang']=='ar' ? 'تم انهاء الطلب بنجاح.' : 'finsih  order.';
                AdminReport($request["user_id"],"قام بانهاء طلب من ".$resev->Service->User->name);
                return response()->json(['key'=>'success', 'value'=>'1','data'=>array("id"=>$resev->id),'msg'=>$msg]);
            }
            else
            {
                $msg =  $request['lang']=='ar' ? 'الحجز غير موجود.' : 'reservation not found.';
                return response()->json(['key' => 'fail','value' => '0','msg'=>$msg]);
            }
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // cancel reervation
    public function cancelReservation(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_id'        => 'required|exists:users,id',
            'reservation_id' => 'required|exists:reservations,id',
        ]);
        if ($validator->passes())
        {
            $resev              = Reservation::where("id",$request["reservation_id"])
                ->where("user_id",$request["user_id"])
                ->where("status",2)->first();
            if($resev)
            {
                $name    = $resev->User->name;
                $service = $resev->Service;
                if(CheckExpireService($service))
                {
                    $service->update(["status"=>1]);
                }
                $resev->delete();
                $msg =  $request['lang']=='ar' ? 'تم الغاء الطلب بنجاح.' : 'done cancel order.';
                AdminReport($request["user_id"],"قام بالالغاء طلب من ".$name);
                return response()->json(['key'=>'success', 'value'=>'1','msg'=>$msg]);
            }
            else
            {
                $msg =  $request['lang']=='ar' ? 'الحجز غير موجود.' : 'reservation not found.';
                return response()->json(['key'=>'fail', 'value'=>'0','data'=>array("id"=>$resev->id),'msg'=>$msg]);
            }
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }

}
