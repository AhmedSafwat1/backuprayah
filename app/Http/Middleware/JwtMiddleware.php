<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['key'=>'fail','value'=>'1','msg'=>"user_not_found"]);
            }
            $request["user_id"] = $user->id;
            return $next($request);
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['key'=>'fail','value'=>'0','msg'=>"token_expired"]);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['key'=>'fail','value'=>'0','msg'=>"token_invalid"]);
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['key'=>'fail','value'=>'0','msg'=>"token_absent"]);
        }

    }
}
