<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    protected $fillable = [
        'sender_id', 'message', 'reciever_id','roomchat_id',
    ];
    // chat room which user as sender start first speak
    public function RoomChat()
    {
        return $this->belongsTo('App\RoomChat ','roomchat_id','id');
    }
    //  user as sender start first speak
    public function Sender()
    {
        return $this->belongsTo('App\User','sender_id','id');
    }
    public function Reciever()
    {
        return $this->belongsTo('App\User','reciever_id','id');
    }

}
